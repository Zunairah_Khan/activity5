package com.example.classactivity5;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.ListView;

public class MainActivity extends AppCompatActivity {

    ListView simpleList;
    String teamList[] = {"Islamabad", "Karachi", "Lahore", "Multan", "Peshawar", "Quetta"};
    int flags[] = {R.mipmap.is, R.mipmap.ka, R.mipmap.la, R.mipmap.mu, R.mipmap.pe, R.mipmap.qu};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        simpleList = (ListView) findViewById(R.id.simpleListView);
        CustomAdapter customAdapter = new CustomAdapter(getApplicationContext(), teamList, flags);
        simpleList.setAdapter(customAdapter);
    }
}